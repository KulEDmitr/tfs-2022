package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.converter.WalletInputConverter;
import com.tinkoff.sirius.financialtracker.converter.WalletResponseConverter;
import com.tinkoff.sirius.financialtracker.exception.WalletNotFoundException;
import com.tinkoff.sirius.financialtracker.model.Ticker;
import com.tinkoff.sirius.financialtracker.model.dto.WalletInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.WalletResponseDto;
import com.tinkoff.sirius.financialtracker.model.dto.WalletsTotalResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.WalletEntity;
import com.tinkoff.sirius.financialtracker.permission.SessionPermission;
import com.tinkoff.sirius.financialtracker.repository.WalletMonthlyTotalsJpqlResponse;
import com.tinkoff.sirius.financialtracker.repository.WalletRepository;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class WalletServiceOnRepository implements WalletService {

    private final WalletRepository walletRepository;
    private final SessionPermission permission;
    private final WalletInputConverter inputConverter;
    private final WalletResponseConverter responseConverter;

    @Override
    @Transactional
    public WalletResponseDto create(WalletInputDto data) {
        WalletEntity newWalletData = inputConverter
            .convert(data)
            .setOwner(permission.getCurrentUser());
        WalletEntity savedWallet = walletRepository.save(newWalletData);
        return responseConverter.convert(savedWallet);
    }

    @Override
    public List<WalletResponseDto> getAll() {
        Long ownerId = permission.getCurrentUser().getId();
        var walletList = walletRepository.findAllByOwnerId(ownerId);
        return walletList.stream()
                .map(responseConverter::convert)
                .collect(Collectors.toList());
    }

    @Override
    public WalletEntity getEntityById(Long walletId) {
        Long sessionUserId = permission.getCurrentUser().getId();
        Optional<WalletEntity> savedWallet =
            walletRepository.findByIdAndOwnerId(walletId, sessionUserId);
        return savedWallet.orElseThrow(WalletNotFoundException::new);
    }

    @Override
    public WalletResponseDto getById(Long walletId) {
        WalletEntity savedWallet = getEntityById(walletId);
        return responseConverter.convert(savedWallet);
    }

    @Override
    @Transactional
    public WalletResponseDto update(Long walletId, WalletInputDto data) {
        WalletEntity savedWallet = getEntityById(walletId);
        WalletEntity wallet = inputConverter
            .convert(data)
            .setId(savedWallet.getId())
            .setOwner(savedWallet.getOwner())
            .setBalance(savedWallet.getBalance());
        walletRepository.save(wallet);

        return responseConverter.convert(wallet);
    }

    @Override
    @Transactional
    public WalletResponseDto delete(Long walletId) {
        WalletEntity savedWallet = getEntityById(walletId);
        walletRepository.delete(savedWallet);
        return responseConverter.convert(savedWallet);
    }

    @Override
    @Transactional
    public WalletsTotalResponseDto getWalletsTotal() {
        Long sessionUserId = permission.getCurrentUser().getId();
        List<WalletMonthlyTotalsJpqlResponse> walletMonthlyTotalsList = walletRepository
            .getMonthlyTotalByOwner(sessionUserId);
        List<WalletEntity> walletEntityList = walletRepository.findAllByOwnerId(sessionUserId);

        BigDecimal monthlyIncome = walletMonthlyTotalsList.stream()
            .map(WalletMonthlyTotalsJpqlResponse::totalIncome)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal monthlyOutcome = walletMonthlyTotalsList.stream()
            .map(WalletMonthlyTotalsJpqlResponse::totalOutcome)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal balance = walletEntityList.stream()
            .map(WalletEntity::getBalance)
            .reduce(BigDecimal.ZERO, BigDecimal::add);

        return new WalletsTotalResponseDto(
            balance,
            monthlyIncome,
            monthlyOutcome,
            Ticker.RUB
        );
    }

    @Override
    @Transactional
    public WalletsTotalResponseDto getWalletTotal(Long walletId) {
        WalletEntity savedWallet = getEntityById(walletId);
        Optional<WalletMonthlyTotalsJpqlResponse> walletMonthlyTotal =
            walletRepository.getMonthlyTotalByOwnerAndWallet(
                savedWallet.getOwner().getId(),
                walletId
        );

        return new WalletsTotalResponseDto(
            savedWallet.getBalance(),
            walletMonthlyTotal.isPresent() ? walletMonthlyTotal.get().totalIncome() : BigDecimal.ZERO,
            walletMonthlyTotal.isPresent() ? walletMonthlyTotal.get().totalOutcome() : BigDecimal.ZERO,
            Ticker.RUB
        );
    }
}
