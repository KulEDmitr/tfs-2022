package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.model.Ticker;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class TickerService {

    public List<Ticker> getAllTickers() {
        return Arrays.asList(Ticker.values());
    }
}
