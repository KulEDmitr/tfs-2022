package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.converter.UserInputConverter;
import com.tinkoff.sirius.financialtracker.converter.UserResponseConverter;
import com.tinkoff.sirius.financialtracker.exception.UserNotFoundException;
import com.tinkoff.sirius.financialtracker.model.dto.UserInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.UserResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import com.tinkoff.sirius.financialtracker.repository.UserRepository;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceOnRepository implements UserService {

    private final UserRepository userRepository;
    private final UserInputConverter inputConverter;
    private final UserResponseConverter responseConverter;

    @Override
    @Transactional
    public UserResponseDto create(UserInputDto data) {
        UserEntity newUserData = inputConverter.convert(data);
        UserEntity savedUser = userRepository.save(newUserData);
        return responseConverter.convert(savedUser);
    }

    @Override
    public UserResponseDto getById(Long userId) {
        UserEntity savedUser = getEntityById(userId);
        return responseConverter.convert(savedUser);
    }

    @Override
    @Transactional
    public UserResponseDto delete(Long userId) {
        UserEntity savedUser = getEntityById(userId);
        userRepository.delete(savedUser);
        return responseConverter.convert(savedUser);
    }

    @Override
    public Optional<UserEntity> getByEmail(String userEmail) {
        return userRepository.findByEmail(userEmail);
    }

    private UserEntity getEntityById(Long userId) {
        Optional<UserEntity> sessionUser = userRepository.findById(userId);
        return sessionUser.orElseThrow(UserNotFoundException::new);
    }
}
