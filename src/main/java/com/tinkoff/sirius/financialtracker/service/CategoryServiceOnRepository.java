package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.converter.CategoryIconResponseConverter;
import com.tinkoff.sirius.financialtracker.converter.CategoryInputConverter;
import com.tinkoff.sirius.financialtracker.converter.CategoryResponseConverter;
import com.tinkoff.sirius.financialtracker.exception.CategoryNotFoundException;
import com.tinkoff.sirius.financialtracker.model.CategoryIcon;
import com.tinkoff.sirius.financialtracker.model.TransactionType;
import com.tinkoff.sirius.financialtracker.model.dto.CategoryInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.CategoryResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.CategoryEntity;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import com.tinkoff.sirius.financialtracker.permission.SessionPermission;
import com.tinkoff.sirius.financialtracker.repository.TransactionsCategoryRepository;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CategoryServiceOnRepository implements CategoryService {

    private final CategoryInputConverter categoryInputConverter;
    private final CategoryResponseConverter categoryResponseConverter;
    private final CategoryIconResponseConverter iconResponseConverter;
    private final TransactionsCategoryRepository transactionsCategoryRepository;
    private final SessionPermission permission;

    @Override
    @Transactional
    public CategoryResponseDto create(CategoryInputDto data) {
        UserEntity sessionUser = permission.getCurrentUser();
        CategoryEntity newCategoryData = categoryInputConverter
            .convert(data)
            .setOwner(sessionUser);
        CategoryEntity savedCategory = transactionsCategoryRepository.save(newCategoryData);
        return categoryResponseConverter.convert(savedCategory);
    }

    @Override
    public CategoryEntity getEntityById(Long categoryId) {
        Long sessionUserId = permission.getCurrentUser().getId();
        Optional<CategoryEntity> savedWallet =
            transactionsCategoryRepository.findByIdAndOwnerId(categoryId, sessionUserId);
        return savedWallet.orElseThrow(CategoryNotFoundException::new);
    }

    @Override
    public CategoryResponseDto getById(Long categoryId) {
        CategoryEntity savedCategory = getEntityById(categoryId);
        return categoryResponseConverter.convert(savedCategory);
    }

    @Override
    public List<CategoryResponseDto> getAllByType(TransactionType type) {
        List<CategoryEntity> defaultCategoriesList =
            transactionsCategoryRepository.findAllByOwnerAndTypeIs(null, type);
        List<CategoryEntity> userCreatedCategoriesList =
            transactionsCategoryRepository.findAllByOwnerAndTypeIs(permission.getCurrentUser(), type);
        return Stream.concat(defaultCategoriesList.stream(), userCreatedCategoriesList.stream())
            .map(categoryResponseConverter::convert)
            .collect(Collectors.toList());
    }

    @Override
    public List<String> getAllIcons() {
        return Arrays.stream(CategoryIcon.values())
            .filter(CategoryIcon::getIsAvailableForPick)
            .map(iconResponseConverter::convert)
            .collect(Collectors.toList());
    }
}
