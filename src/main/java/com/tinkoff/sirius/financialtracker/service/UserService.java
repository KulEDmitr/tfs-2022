package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.model.dto.UserInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.UserResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import java.util.Optional;

public interface UserService {

    UserResponseDto create(UserInputDto data);

    UserResponseDto getById(Long userId);
    Optional<UserEntity> getByEmail(String userEmail);

    UserResponseDto delete(Long userId);
}
