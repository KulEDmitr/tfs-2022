package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.converter.TransactionInputConverter;
import com.tinkoff.sirius.financialtracker.converter.TransactionResponseConverter;
import com.tinkoff.sirius.financialtracker.exception.CategoryNotFoundException;
import com.tinkoff.sirius.financialtracker.exception.TransactionNotFoundException;
import com.tinkoff.sirius.financialtracker.exception.WalletNotFoundException;
import com.tinkoff.sirius.financialtracker.model.dto.TransactionInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.TransactionResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.CategoryEntity;
import com.tinkoff.sirius.financialtracker.model.entity.TransactionEntity;
import com.tinkoff.sirius.financialtracker.model.entity.WalletEntity;
import com.tinkoff.sirius.financialtracker.permission.SessionPermission;
import com.tinkoff.sirius.financialtracker.repository.TransactionRepository;
import com.tinkoff.sirius.financialtracker.repository.TransactionsCategoryRepository;
import com.tinkoff.sirius.financialtracker.repository.WalletRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class TransactionServiceOnRepository implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final WalletRepository walletRepository;
    private final TransactionsCategoryRepository categoryRepository;
    private final TransactionInputConverter inputConverter;
    private final TransactionResponseConverter responseConverter;
    private final WalletBalanceManager walletBalanceManager;
    private final SessionPermission permission;

    @Override
    @Transactional
    public TransactionResponseDto create(TransactionInputDto data) {
        Long sessionUserId = permission.getCurrentUser().getId();
        Optional<WalletEntity> walletEntity =
            walletRepository.findByIdAndOwnerId(data.getWalletId(), sessionUserId);
        walletEntity.orElseThrow(WalletNotFoundException::new);

        Optional<CategoryEntity> categoryEntity =
            categoryRepository.findByIdAndOwnerId(data.getCategoryId(), sessionUserId);
        categoryEntity.orElseThrow(CategoryNotFoundException::new);

        TransactionEntity newTransactionData = inputConverter
            .convert(data)
            .setWallet(walletEntity.get())
            .setCategory(categoryEntity.get());
        TransactionEntity savedTransaction = transactionRepository.save(newTransactionData);

        WalletEntity walletWithUpdatedBalance = walletBalanceManager.addTransactionAmountToWalletBalance(
            walletEntity.get(), savedTransaction
        );
        walletRepository.save(walletWithUpdatedBalance);
        return responseConverter.convert(savedTransaction);
    }

    @Override
    public List<TransactionResponseDto> getAllByWalletId(Long walletId) {
        Long sessionUserId = permission.getCurrentUser().getId();
        List<TransactionEntity> transactionList =
            transactionRepository.findAllByWalletIdAndWalletOwnerId(walletId, sessionUserId);
        return transactionList.stream()
            .map(responseConverter::convert).collect(Collectors.toList());
    }

    public TransactionEntity getEntityById(Long transactionId) {
        Long sessionUserId = permission.getCurrentUser().getId();
        Optional<TransactionEntity> transactionEntity =
            transactionRepository.findByIdAndWalletOwnerId(transactionId, sessionUserId);
        return transactionEntity.orElseThrow(TransactionNotFoundException::new);
    }

    @Override
    public TransactionResponseDto getById(Long transactionId) {
        TransactionEntity savedTransaction = getEntityById(transactionId);
        return responseConverter.convert(savedTransaction);
    }

    @Override
    @Transactional
    public TransactionResponseDto update(Long transactionId, TransactionInputDto data) {
        Long sessionUserId = permission.getCurrentUser().getId();
        TransactionEntity transactionEntity = getEntityById(transactionId);
        Optional<CategoryEntity> categoryEntity =
            categoryRepository.findByIdAndOwnerId(data.getCategoryId(), sessionUserId);
        categoryEntity.orElseThrow(CategoryNotFoundException::new);

        WalletEntity wallet = transactionEntity.getWallet();
        WalletEntity walletWithRemovedBalance = walletBalanceManager.removeTransactionAmountFromWalletBalance(
            wallet, transactionEntity
        );

        TransactionEntity newTransaction = inputConverter
            .convert(data)
            .setId(transactionEntity.getId())
            .setCategory(categoryEntity.get())
            .setWallet(transactionEntity.getWallet());

        TransactionEntity savedTransaction = transactionRepository.save(newTransaction);

        WalletEntity walletWithUpdatedBalance = walletBalanceManager.addTransactionAmountToWalletBalance(
            walletWithRemovedBalance, newTransaction
        );
        walletRepository.save(walletWithUpdatedBalance);

        return responseConverter.convert(savedTransaction);
    }

    @Override
    @Transactional
    public TransactionResponseDto delete(Long transactionId) {
        TransactionEntity savedTransaction = getEntityById(transactionId);
        transactionRepository.delete(savedTransaction);

        WalletEntity walletWithUpdatedBalance = walletBalanceManager.removeTransactionAmountFromWalletBalance(
            savedTransaction.getWallet(), savedTransaction
        );
        walletRepository.save(walletWithUpdatedBalance);

        return responseConverter.convert(savedTransaction);
    }
}
