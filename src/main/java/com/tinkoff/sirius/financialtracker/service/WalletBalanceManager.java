package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.model.entity.TransactionEntity;
import com.tinkoff.sirius.financialtracker.model.entity.WalletEntity;
import java.math.BigDecimal;
import org.springframework.stereotype.Component;

@Component
public class WalletBalanceManager {
    public WalletEntity addTransactionAmountToWalletBalance(
        WalletEntity walletEntity,
        TransactionEntity transactionEntity
    ) {
        BigDecimal currentBalance = walletEntity.getBalance();
        BigDecimal updatedBalance = currentBalance.add(transactionEntity.getSignedAmount());
        return walletEntity.withBalance(updatedBalance);
    }

    public WalletEntity removeTransactionAmountFromWalletBalance(
        WalletEntity walletEntity,
        TransactionEntity transactionEntity
    ) {
        BigDecimal currentBalance = walletEntity.getBalance();
        BigDecimal updatedBalance = currentBalance.subtract(transactionEntity.getSignedAmount());
        return walletEntity.withBalance(updatedBalance);
    }
}
