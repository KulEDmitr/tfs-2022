package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.model.dto.TransactionInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.TransactionResponseDto;
import java.util.List;

public interface TransactionService {
    TransactionResponseDto create(TransactionInputDto data);

    List<TransactionResponseDto> getAllByWalletId(Long walletId);

    TransactionResponseDto getById(Long transactionId);

    TransactionResponseDto update(Long transactionId, TransactionInputDto data);

    TransactionResponseDto delete(Long transactionId);
}
