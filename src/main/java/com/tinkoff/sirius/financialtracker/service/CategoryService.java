package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.model.TransactionType;
import com.tinkoff.sirius.financialtracker.model.dto.CategoryInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.CategoryResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.CategoryEntity;
import java.util.List;

public interface CategoryService {

    CategoryResponseDto create(CategoryInputDto data);

    List<CategoryResponseDto> getAllByType(TransactionType type);

    CategoryResponseDto getById(Long categoryId);
    public CategoryEntity getEntityById(Long categoryId);

    List<String> getAllIcons();
}
