package com.tinkoff.sirius.financialtracker.service;

import com.tinkoff.sirius.financialtracker.model.dto.WalletInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.WalletResponseDto;
import com.tinkoff.sirius.financialtracker.model.dto.WalletsTotalResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import com.tinkoff.sirius.financialtracker.model.entity.WalletEntity;
import java.util.List;

public interface WalletService {

    WalletResponseDto create(WalletInputDto data);

    List<WalletResponseDto> getAll();

    WalletResponseDto getById(Long walletId);
    WalletEntity getEntityById(Long walletId);

    WalletResponseDto update(Long walletId, WalletInputDto data);

    WalletResponseDto delete(Long walletId);
    WalletsTotalResponseDto getWalletsTotal();
    WalletsTotalResponseDto getWalletTotal(Long walletId);
}
