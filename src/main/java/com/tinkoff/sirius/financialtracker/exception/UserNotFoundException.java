package com.tinkoff.sirius.financialtracker.exception;

public class UserNotFoundException extends ClientMessageException {
    private final static String MESSAGE = "Unable to find wallet with given parameters";

    public UserNotFoundException() {
        super(MESSAGE);
    }
}
