package com.tinkoff.sirius.financialtracker.exception;

public class CategoryNotFoundException extends ClientMessageException {

    private final static String MESSAGE = "Unable to find transaction category with given parameters";

    public CategoryNotFoundException() {
        super(MESSAGE);
    }
}
