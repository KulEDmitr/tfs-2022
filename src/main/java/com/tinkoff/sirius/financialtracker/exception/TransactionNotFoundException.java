package com.tinkoff.sirius.financialtracker.exception;

public class TransactionNotFoundException extends ClientMessageException {
    private final static String MESSAGE = "Unable to find transaction with given parameters";

    public TransactionNotFoundException() {
        super(MESSAGE);
    }
}
