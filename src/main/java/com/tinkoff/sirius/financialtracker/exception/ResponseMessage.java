package com.tinkoff.sirius.financialtracker.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class ResponseMessage {
    private final String message;
}