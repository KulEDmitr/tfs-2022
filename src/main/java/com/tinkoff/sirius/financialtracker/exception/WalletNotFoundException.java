package com.tinkoff.sirius.financialtracker.exception;

public class WalletNotFoundException extends ClientMessageException {

    private final static String MESSAGE = "Unable to find wallet with given parameters";

    public WalletNotFoundException() {
        super(MESSAGE);
    }
}
