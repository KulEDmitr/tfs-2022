package com.tinkoff.sirius.financialtracker.exception;

public class ValidationUserException extends ClientMessageException {

    private final static String MESSAGE = "Access denied";

    public ValidationUserException() {
        super(MESSAGE);
    }

}
