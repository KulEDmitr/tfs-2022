package com.tinkoff.sirius.financialtracker.repository;

import com.tinkoff.sirius.financialtracker.model.entity.TransactionEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Long> {

    List<TransactionEntity> findAllByWalletIdAndWalletOwnerId(@Param("walletId") Long walletId,
        @Param("walletOwnerId") Long walletOwnerId);

    Optional<TransactionEntity> findByIdAndWalletOwnerId(@Param("id") Long id,
        @Param("walletOwnerId") Long walletOwnerId);
}
