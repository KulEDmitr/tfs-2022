package com.tinkoff.sirius.financialtracker.repository;

import com.tinkoff.sirius.financialtracker.model.entity.WalletEntity;
import java.math.BigDecimal;

public record WalletMonthlyTotalsJpqlResponse(
    WalletEntity walletEntity,
    BigDecimal totalIncome,
    BigDecimal totalOutcome
) {

}
