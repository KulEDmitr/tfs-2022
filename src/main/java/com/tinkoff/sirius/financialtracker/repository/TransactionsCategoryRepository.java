package com.tinkoff.sirius.financialtracker.repository;

import com.tinkoff.sirius.financialtracker.model.TransactionType;
import com.tinkoff.sirius.financialtracker.model.entity.CategoryEntity;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionsCategoryRepository extends JpaRepository<CategoryEntity, Long> {

    List<CategoryEntity> findAllByOwnerAndTypeIs(UserEntity owner, TransactionType type);

    Optional<CategoryEntity> findByIdAndOwnerId(@Param("id") Long id,
        @Param("ownerId") Long ownerId);
}
