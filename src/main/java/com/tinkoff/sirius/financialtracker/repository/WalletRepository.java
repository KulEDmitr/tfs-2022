package com.tinkoff.sirius.financialtracker.repository;

import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import com.tinkoff.sirius.financialtracker.model.entity.WalletEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletRepository extends JpaRepository<WalletEntity, Long> {
    @Query("select "
        + "     new com.tinkoff.sirius.financialtracker.repository.WalletMonthlyTotalsJpqlResponse("
        + "         w,"
        + "         SUM(CASE WHEN c.type = 'INCOME' THEN t.amount ELSE 0 END),"
        + "         SUM(CASE WHEN c.type = 'OUTCOME' THEN t.amount ELSE 0 END)"
        + "     ) "
        + "from transaction t"
        + "     inner join wallet w on t.wallet = w.id "
        + "     inner join category c on t.category = c.id "
        + "where year(t.createdTime) = year(current_date) "
        + "     and month(t.createdTime) = month(current_date) "
        + "     and w.owner.id = :user_id "
        + "group by w"
    )
    List<WalletMonthlyTotalsJpqlResponse> getMonthlyTotalByOwner(@Param("user_id") Long userId);

    @Query("select "
        + "     new com.tinkoff.sirius.financialtracker.repository.WalletMonthlyTotalsJpqlResponse("
        + "         w,"
        + "         SUM(CASE WHEN c.type = 'INCOME' THEN t.amount ELSE 0 END),"
        + "         SUM(CASE WHEN c.type = 'OUTCOME' THEN t.amount ELSE 0 END)"
        + "     ) "
        + "from transaction t"
        + "     inner join wallet w on t.wallet = w.id "
        + "     inner join category c on t.category = c.id "
        + "where w.id = :wallet_id and "
        + "     year(t.createdTime) = year(current_date) "
        + "     and month(t.createdTime) = month(current_date) "
        + "     and w.owner.id = :user_id "
        + "group by w"
    )
    Optional<WalletMonthlyTotalsJpqlResponse> getMonthlyTotalByOwnerAndWallet(
        @Param("user_id") Long userId,
        @Param("wallet_id") Long walletId
    );

    List<WalletEntity> findAllByOwnerId(@Param("ownerId") Long ownerId);

    Optional<WalletEntity> findByIdAndOwnerId(@Param("id") Long id,
        @Param("ownerId") Long ownerId);
}
