package com.tinkoff.sirius.financialtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tfs2022Application {

    public static void main(String[] args) {
        SpringApplication.run(Tfs2022Application.class, args);
    }

}
