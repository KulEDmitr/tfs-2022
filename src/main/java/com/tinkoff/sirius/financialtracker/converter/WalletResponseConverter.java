package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.dto.WalletResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.WalletEntity;
import java.math.BigDecimal;
import org.springframework.stereotype.Component;

@Component
public class WalletResponseConverter implements Converter<WalletEntity, WalletResponseDto> {

    @Override
    public WalletResponseDto convert(WalletEntity wallet) {
        return new WalletResponseDto()
            .setId(wallet.getId())
            .setName(wallet.getName())
            .setBalance(wallet.getBalance())
            .setTicker(wallet.getTicker())
            .setLimit(wallet.getSpendLimit())
            .setIsLimitReached(false)
            .setBalance(BigDecimal.ZERO)
            .setIsHidden(wallet.getIsHidden());
    }
}
