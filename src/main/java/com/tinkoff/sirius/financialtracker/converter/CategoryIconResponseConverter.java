package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.CategoryIcon;
import org.springframework.stereotype.Component;

@Component
public class CategoryIconResponseConverter implements Converter<CategoryIcon, String> {

    @Override
    public String convert(CategoryIcon categoryIcon) {
        return categoryIcon.getAppleIconId();
    }
}
