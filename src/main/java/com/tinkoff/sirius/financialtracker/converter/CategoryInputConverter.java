package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.TransactionType;
import com.tinkoff.sirius.financialtracker.model.dto.CategoryInputDto;
import com.tinkoff.sirius.financialtracker.model.entity.CategoryEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CategoryInputConverter implements Converter<CategoryInputDto, CategoryEntity> {

    private final CategoryIconInputConverter categoryIconInputConverter;

    @Override
    public CategoryEntity convert(CategoryInputDto dto) {
        return new CategoryEntity()
            .setName(dto.getName())
            .setIcon(
                categoryIconInputConverter.convert(dto.getIcon())
            )
            .setColor(dto.getColor())
            .setType(TransactionType.valueOf(dto.getType().name()));
    }
}
