package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.dto.WalletInputDto;
import com.tinkoff.sirius.financialtracker.model.entity.WalletEntity;
import org.springframework.stereotype.Component;

@Component
public class WalletInputConverter implements Converter<WalletInputDto, WalletEntity> {

    private final TickerInputConverter tickerInputConverter;

    public WalletInputConverter(TickerInputConverter tickerInputConverter) {
        this.tickerInputConverter = tickerInputConverter;
    }

    @Override
    public WalletEntity convert(WalletInputDto dto) {
        return new WalletEntity()
            .setName(dto.getName())
            .setTicker(
                tickerInputConverter.convert(dto.getTicker())
            )
            .setSpendLimit(dto.getLimit())
            .setIsHidden(dto.getIsHidden());
    }
}
