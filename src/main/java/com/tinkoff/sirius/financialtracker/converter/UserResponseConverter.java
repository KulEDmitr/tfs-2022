package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.dto.UserResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserResponseConverter implements Converter<UserEntity, UserResponseDto> {

    @Override
    public UserResponseDto convert(UserEntity user) {
        return new UserResponseDto()
            .setId(user.getId())
            .setEmail(user.getEmail())
            .setAuthId(user.getAuthId());
    }
}
