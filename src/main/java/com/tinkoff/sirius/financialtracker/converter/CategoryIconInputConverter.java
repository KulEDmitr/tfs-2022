package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.CategoryIcon;
import java.util.Arrays;
import org.springframework.stereotype.Component;

@Component
public class CategoryIconInputConverter implements Converter<String, CategoryIcon> {

    @Override
    public CategoryIcon convert(String categoryIconDto) {
        return Arrays.stream(CategoryIcon.values())
            .filter(it -> it.getAppleIconId().equals(categoryIconDto))
            .findFirst().orElse(CategoryIcon.DEFAULT);
    }
}
