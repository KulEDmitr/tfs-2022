package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.Ticker;
import org.springframework.stereotype.Component;

@Component
public class TickerInputConverter implements Converter<String, Ticker> {

    @Override
    public Ticker convert(String data) {
        return Ticker.valueOf(data);
    }
}
