package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.dto.UserInputDto;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserInputConverter implements Converter<UserInputDto, UserEntity> {

    @Override
    public UserEntity convert(UserInputDto dto) {
        return new UserEntity()
            .setEmail(dto.getEmail())
            .setAuthId(dto.getAuthId());
    }
}
