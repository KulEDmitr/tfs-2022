package com.tinkoff.sirius.financialtracker.converter;

interface Converter<I, O> {
    O convert(I data);
}
