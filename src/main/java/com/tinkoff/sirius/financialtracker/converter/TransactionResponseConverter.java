package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.dto.TransactionResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.TransactionEntity;
import org.springframework.stereotype.Component;

@Component
public class TransactionResponseConverter implements Converter<TransactionEntity, TransactionResponseDto> {

    @Override
    public TransactionResponseDto convert(TransactionEntity transaction) {
        return new TransactionResponseDto()
            .setId(transaction.getId())
            .setAmount(transaction.getAmount())
            .setCategoryId(transaction.getCategory().getId())
            .setWalletId(transaction.getWallet().getId())
            .setCreatedTime(transaction.getCreatedTime());
    }
}
