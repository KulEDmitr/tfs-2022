package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.dto.CategoryResponseDto;
import com.tinkoff.sirius.financialtracker.model.entity.CategoryEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CategoryResponseConverter implements Converter<CategoryEntity, CategoryResponseDto> {

    private final CategoryIconResponseConverter categoryIconResponseConverter;

    @Override
    public CategoryResponseDto convert(CategoryEntity category) {
        return new CategoryResponseDto()
            .setId(category.getId())
            .setName(category.getName())
            .setIcon(categoryIconResponseConverter.convert(category.getIcon()))
            .setColor(category.getColor())
            .setType(category.getType());
    }
}
