package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.dto.TransactionInputDto;
import com.tinkoff.sirius.financialtracker.model.entity.TransactionEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TransactionInputConverter implements Converter<TransactionInputDto, TransactionEntity> {

    @Override
    public TransactionEntity convert(TransactionInputDto dto) {
        return new TransactionEntity()
            .setAmount(dto.getAmount())
            .setCreatedTime(dto.getCreatedTime());
    }
}
