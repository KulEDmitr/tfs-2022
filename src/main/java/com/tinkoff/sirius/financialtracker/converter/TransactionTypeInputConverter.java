package com.tinkoff.sirius.financialtracker.converter;

import com.tinkoff.sirius.financialtracker.model.TransactionType;

public class TransactionTypeInputConverter implements Converter<String, TransactionType> {

    @Override
    public TransactionType convert(String data) {
        return TransactionType.valueOf(data);
    }
}
