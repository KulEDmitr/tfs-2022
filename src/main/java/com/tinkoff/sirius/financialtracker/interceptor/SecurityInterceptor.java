package com.tinkoff.sirius.financialtracker.interceptor;


import com.tinkoff.sirius.financialtracker.exception.ValidationUserException;
import com.tinkoff.sirius.financialtracker.model.Session;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import com.tinkoff.sirius.financialtracker.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@RequiredArgsConstructor
@Component
public class SecurityInterceptor implements HandlerInterceptor {
    private final UserService userService;
    private final Session session;

    @Override
    public boolean preHandle(
        @NonNull HttpServletRequest request,
        @NonNull HttpServletResponse response,
        @NonNull Object handler) throws Exception {
        String userEmail = request.getHeader("userEmail");
        if (userEmail == null) {
            log.debug("User email not stated in header parameter userEmail. Required for authorized zone");
            return HandlerInterceptor.super.preHandle(request, response, handler);
        }
        UserEntity authorizedUserEntity = userService.getByEmail(userEmail)
                .orElseThrow(() -> {
                    log.debug("Unable to authorize user with email {}, relevant user not found", userEmail);
                    throw new ValidationUserException();
                });
        session.setUserEntity(authorizedUserEntity);
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(
        @NonNull HttpServletRequest request,
        @NonNull HttpServletResponse response,
        @NonNull Object handler,
        @Nullable ModelAndView modelAndView) {
        log.debug("Here we are in post handle");
    }
}
