package com.tinkoff.sirius.financialtracker.model;

public enum Ticker {
    USD,
    EUR,
    GBP,
    RUB,
}
