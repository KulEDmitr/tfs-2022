package com.tinkoff.sirius.financialtracker.model.entity;

import com.sun.istack.NotNull;
import com.tinkoff.sirius.financialtracker.model.CategoryIcon;
import com.tinkoff.sirius.financialtracker.model.TransactionType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Table
@Entity(name = "category")
public class CategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_seq")
    @SequenceGenerator(name = "category_seq", sequenceName = "category_seq", allocationSize = 1)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType type;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private CategoryIcon icon;
    @Column(nullable = false)
    private String color;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    @NotNull
    private UserEntity owner;
}
