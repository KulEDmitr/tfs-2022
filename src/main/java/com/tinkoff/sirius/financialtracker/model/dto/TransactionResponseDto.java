package com.tinkoff.sirius.financialtracker.model.dto;

import java.math.BigDecimal;
import java.time.Instant;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class TransactionResponseDto implements DtoResponseInterface {

    private Long id;
    private BigDecimal amount;
    private Long walletId;
    private Long categoryId;
    private Instant createdTime;
}
