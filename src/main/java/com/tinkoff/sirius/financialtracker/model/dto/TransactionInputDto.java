package com.tinkoff.sirius.financialtracker.model.dto;

import java.math.BigDecimal;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class TransactionInputDto implements DtoInputInterface {

    @Positive(message = "Значение суммы должно быть положительным")
    @NotNull(message = "Необходимо задать сумму операции")
    private BigDecimal amount;

    @Positive(message = "Идентификатор обязан быть положительным")
    @NotNull(message = "Необходимо задать идентификатор категории")
    private Long categoryId;

    @PastOrPresent(message = "Невозможно создать операцию в будущем")
    @NotNull(message = "Необходимо задать дату и время совершения операции")
    private Instant createdTime;

    @NotNull(message = "Необходимо указать id кошелька")
    private Long walletId;
}
