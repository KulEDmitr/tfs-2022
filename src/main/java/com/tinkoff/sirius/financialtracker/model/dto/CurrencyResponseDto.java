package com.tinkoff.sirius.financialtracker.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CurrencyResponseDto implements DtoResponseInterface {

    private Long id;
    private String ticker;
    private String sign;
    private String name;
}
