package com.tinkoff.sirius.financialtracker.model.dto;

import com.tinkoff.sirius.financialtracker.model.TransactionType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CategoryInputDto implements DtoInputInterface {

    @NotBlank(message = "Название категории должно содержать хотя бы один непробельный символ")
    private String name;
    @NotNull(message = "Необходимо выбрать тип операций для которого создается категория")
    private TransactionType type;
    @NotNull(message = "Необходимо выбрать иконку")
    private String icon;
    @NotNull(message = "Необходимо выбрать цвет")
    private String color;
}
