package com.tinkoff.sirius.financialtracker.model.entity;

import com.sun.istack.NotNull;
import com.tinkoff.sirius.financialtracker.model.Ticker;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.With;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Table
@Entity(name = "wallet")
@AllArgsConstructor
@NoArgsConstructor
public class WalletEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_seq")
    @SequenceGenerator(name = "wallet_seq", sequenceName = "wallet_seq", allocationSize = 1)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    @With
    private BigDecimal balance = BigDecimal.ZERO;
    @Column(nullable = false)
    private String name;
    @Column(name = "spend_limit")
    private BigDecimal spendLimit;
    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Ticker ticker;
    @Column(nullable = false)
    private Boolean isHidden;
    @ManyToOne
    @JoinColumn(name = "account_id")
    @NotNull
    private UserEntity owner;
}
