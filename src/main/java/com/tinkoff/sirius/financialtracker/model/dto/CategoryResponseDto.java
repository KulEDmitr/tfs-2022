package com.tinkoff.sirius.financialtracker.model.dto;

import com.tinkoff.sirius.financialtracker.model.TransactionType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CategoryResponseDto implements DtoResponseInterface {

    private Long id;
    private String name;
    private TransactionType type;
    private String icon;
    private String color;
}
