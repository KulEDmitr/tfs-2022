package com.tinkoff.sirius.financialtracker.model;

import lombok.Getter;

@Getter
public enum CategoryIcon {
    CREDIT_CARD_ICON("credit-card-icon", false),
    GIFT_ICON("gift-icon", false),
    PERCENT_ICON("percent-icon", false),
    BASKET_ICON("basket-icon", false),
    DRUG_ICON("drug-icon", false),
    FORK_AND_KNIFE_ICON("fork-and-knife-icon", false),
    HOUSE_ICON("house-icon", false),
    PALM_ICON("palm-icon", false),
    PETROL_ICON("petrol-icon", false),
    SHUTTLECOCK_ICON("shuttlecock-icon", false),
    TRAIN_ICON("train-icon", false),

    TRASH_CIRCLE_FILL("trash.circle.fill"),
    DIAMOND_CIRCLE_FILL("diamond.circle.fill"),
    FORK_KNIFE_CIRCLE_FILL("fork.knife.circle.fill"),
    PILLS_CIRCLE_FILL("pills.circle.fill"),
    PERSONALHOTSPOT_CIRCLE_FILL("personalhotspot.circle.fill"),
    ANTENNA_RADIOWAVES_LEFT_AND_RIGHT_CIRCLE_FILL("antenna.radiowaves.left.and.right.circle.fill"),
    THEATERMASKS_CIRCLE_FILL("theatermasks.circle.fill"),
    BOLT_CAR_CIRCLE_FILL("bolt.car.circle.fill"),
    PAWPRINT_CIRCLE_FILL("pawprint.circle.fill"),
    SHUFFLE_CIRCLE_FILL("shuffle.circle.fill"),
    PAPERPLANE_CIRCLE_FILL("paperplane.circle.fill"),
    CREDITCARD_CIRCLE_FILL("creditcard.circle.fill"),
    TRAY_CIRCLE_FILL("tray.circle.fill"),
    HAND_RAISED_CIRCLE_FILL("hand.raised.circle.fill"),
    HEADPHONES_CIRCLE_FILL("headphones.circle.fill"),
    GRADUATIONCAP_CIRCLE_FILL("graduationcap.circle.fill"),
    GIFT_CIRCLE_FILL("gift.circle.fill"),
    PHONE_CIRCLE_FILL("phone.circle.fill"),
    SMALLCIRCLE_FILLED_CIRCLE_FILL("smallcircle.filled.circle.fill"),
    FILM_CIRCLE_FILL("film.circle.fill"),
    CAR_CIRCLE_FILL("car.circle.fill"),
    CROSS_CIRCLE_FILL("cross.circle.fill"),
    CALENDAR_CIRCLE_FILL("calendar.circle.fill"),
    TV_CIRCLE_FILL("tv.circle.fill"),
    BRIEFCASE_CIRCLE_FILL("briefcase.circle.fill"),
    FOLDER_CIRCLE_FILL("folder.circle.fill"),
    COLONCURRENCYSIGN_CIRCLE_FILL("coloncurrencysign.circle.fill"),
    FLAME_CIRCLE_FILL("flame.circle.fill"),
    STETHOSCOPE_CIRCLE_FILL("stethoscope.circle.fill");

    public final static CategoryIcon DEFAULT = CategoryIcon.TRASH_CIRCLE_FILL;

    private final String appleIconId;
    private final Boolean isAvailableForPick;

    CategoryIcon(String appleIconId, boolean isAvailableForPick) {
        this.appleIconId = appleIconId;
        this.isAvailableForPick = isAvailableForPick;
    }

    CategoryIcon(String appleIconId) {
        this.appleIconId = appleIconId;
        this.isAvailableForPick = false;
    }
}
