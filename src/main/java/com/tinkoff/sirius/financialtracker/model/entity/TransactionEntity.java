package com.tinkoff.sirius.financialtracker.model.entity;

import com.sun.istack.NotNull;
import com.tinkoff.sirius.financialtracker.model.TransactionType;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Table
@Entity(name = "transaction")
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_seq")
    @SequenceGenerator(name = "transaction_seq", sequenceName = "transaction_seq", allocationSize = 1)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private BigDecimal amount;
    @ManyToOne
    @JoinColumn(name = "wallet_id")
    @NotNull
    private WalletEntity wallet;
    @ManyToOne
    @JoinColumn(name = "category_id")
    @NotNull
    private CategoryEntity category;
    @Column(nullable = false)
    private Instant createdTime;

    public BigDecimal getSignedAmount() {
        return this.category.getType() == TransactionType.OUTCOME ? amount.negate() : amount;
    }
}
