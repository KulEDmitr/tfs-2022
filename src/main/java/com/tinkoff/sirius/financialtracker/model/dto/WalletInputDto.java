package com.tinkoff.sirius.financialtracker.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Модель для создания кошелька")
public class WalletInputDto implements DtoInputInterface {

    @Schema(description = "Имя кошелька", example = "Новый кошелек")
    @NotBlank(message = "Название кошелька должно содержать хотя бы один непробельный символ")
    private String name;

    @Schema(description = "Месячный лимит для трат", example = "10000")
    @PositiveOrZero(message = "Отрицательные значения лимита не поддерживаются")
    private BigDecimal limit;

    @Schema(description = "Идентификатор используемой валюты")
    @NotNull(message = "Необходимо выбрать валюту")
    private String ticker;

    @Schema(description = "Флаг скрытия кошелька", example = "false", defaultValue = "false")
    private Boolean isHidden = false;
}
