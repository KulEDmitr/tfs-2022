package com.tinkoff.sirius.financialtracker.model.dto;

import com.tinkoff.sirius.financialtracker.model.Ticker;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class WalletsTotalResponseDto implements DtoResponseInterface {

    @Schema(description = "Total balance", example = "123")
    private final BigDecimal balance;
    @Schema(description = "Monthly income", example = "123")
    private final BigDecimal monthlyIncome;
    @Schema(description = "Monthly outcome", example = "123")
    private final BigDecimal monthlyOutcome;
    @Schema(description = "Идентификатор используемой валюты")
    private final Ticker ticker;
}
