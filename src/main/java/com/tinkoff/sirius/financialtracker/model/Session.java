package com.tinkoff.sirius.financialtracker.model;

import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
@Getter
@Setter
public class Session {
    private UserEntity userEntity;
}
