package com.tinkoff.sirius.financialtracker.model;

public enum TransactionType {
    INCOME,
    OUTCOME
}
