package com.tinkoff.sirius.financialtracker.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Модель для получения пользователя")
public class UserResponseDto implements DtoResponseInterface {

    @Schema(description = "Google Email пользователя", example = "jonDoe@gmail.com")
    private String email;
    private Long authId;
    private Long id;
}
