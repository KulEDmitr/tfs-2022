package com.tinkoff.sirius.financialtracker.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import com.tinkoff.sirius.financialtracker.model.Ticker;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class WalletResponseDto implements DtoResponseInterface {

    private Long id;

    @Schema(description = "Имя кошелька", example = "Новый кошелек")
    private String name;

    @Schema(description = "Баланс кошелька", example = "1000000")
    private BigDecimal balance;
    @Schema(description = "Месячный лимит для трат", example = "1000000")
    private BigDecimal limit;

    @Schema(description = "Флаг достижения лимита", example = "false")
    private Boolean isLimitReached;
    private Boolean isHidden;
    @Schema(description = "Идентификатор используемой валюты")
    private Ticker ticker;
}
