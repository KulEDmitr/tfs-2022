package com.tinkoff.sirius.financialtracker.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@Schema(description = "Модель для создания пользователя")
public class UserInputDto implements DtoInputInterface {

    @NotBlank
    @Email
    @Schema(description = "Google Email пользователя", example = "jonDoe@gmail.com", required = true)
    private String email;

    @NotNull
    @Positive
    @Schema(description = "Auth Id", example = "123", required = true)
    private Long authId;
}
