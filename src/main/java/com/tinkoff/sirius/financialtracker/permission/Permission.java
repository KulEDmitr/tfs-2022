package com.tinkoff.sirius.financialtracker.permission;

import com.tinkoff.sirius.financialtracker.exception.ClientMessageException;

public interface Permission {
    public void checkPermission() throws ClientMessageException;

}
