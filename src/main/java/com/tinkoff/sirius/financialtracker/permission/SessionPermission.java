package com.tinkoff.sirius.financialtracker.permission;

import com.tinkoff.sirius.financialtracker.exception.ValidationUserException;
import com.tinkoff.sirius.financialtracker.model.Session;
import com.tinkoff.sirius.financialtracker.model.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SessionPermission implements Permission {

    private final Session session;

    public UserEntity getCurrentUser() {
        checkPermission();
        return session.getUserEntity();
    }

    @Override
    public void checkPermission() {
        if (session == null || session.getUserEntity() == null) {
            throw new ValidationUserException();
        }
    }
}
