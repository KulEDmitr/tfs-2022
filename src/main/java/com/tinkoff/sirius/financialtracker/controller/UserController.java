package com.tinkoff.sirius.financialtracker.controller;

import com.tinkoff.sirius.financialtracker.model.dto.UserInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.UserResponseDto;
import com.tinkoff.sirius.financialtracker.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;

    @Operation(summary = "Метод для создания профиля пользователя")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
        description = "Данные пользователя для добавления",
        required = true,
        content = @Content(
            schema = @Schema(implementation = UserInputDto.class)
        )
    )
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponseDto> createUser(@Valid @RequestBody UserInputDto data) {
        return new ResponseEntity<>(userService.create(data), HttpStatus.CREATED);
    }


    @Operation(summary = "Метод для получения профиля пользователя")
    @Parameter(description = "id пользователя для поиска", name = "id", required = true, example = "1")
    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDto> getUser(
        @RequestHeader(value="userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long id) {
        return ResponseEntity.ok(userService.getById(id));
    }

    @Operation(summary = "Метод для удаления профиля пользователя")
    @Parameter(description = "id пользователя для удаления", name = "id", required = true, example = "1")
    @DeleteMapping("/{id}")
    public ResponseEntity<UserResponseDto> deleteUser(
        @RequestHeader(value="userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long id) {
        return ResponseEntity.ok(userService.delete(id));
    }
}
