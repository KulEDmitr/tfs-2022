package com.tinkoff.sirius.financialtracker.controller;

import com.tinkoff.sirius.financialtracker.model.Ticker;
import com.tinkoff.sirius.financialtracker.service.TickerService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/tickers", produces = MediaType.APPLICATION_JSON_VALUE)
public class TickerController {

    private final TickerService tickerService;

    @Operation(summary = "Метод для получения всех тикеров")
    @GetMapping
    public ResponseEntity<List<Ticker>> createTransaction(
        @RequestHeader(value = "userEmail") String userEmail) {
        return ResponseEntity.ok(tickerService.getAllTickers());
    }
}
