package com.tinkoff.sirius.financialtracker.controller;

import com.tinkoff.sirius.financialtracker.model.dto.TransactionInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.TransactionResponseDto;
import com.tinkoff.sirius.financialtracker.service.TransactionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping(value = "/transactions", produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionController {

    private final TransactionService transactionService;

    @Operation(summary = "Метод для создания операции")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
        description = "Данные операции для создания",
        required = true,
        content = @Content(
            schema = @Schema(implementation = TransactionInputDto.class)
        )
    )
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionResponseDto> createTransaction(
        @RequestHeader(value = "userEmail") String userEmail,
        @Valid @RequestBody TransactionInputDto data) {
        return new ResponseEntity<>(transactionService.create(data), HttpStatus.CREATED);
    }

    @Operation(summary = "Метод для получения детальной информации об операции")
    @Parameter(description = "id операции для поиска", name = "id", required = true, example = "1")
    @GetMapping("/{id}")
    public ResponseEntity<TransactionResponseDto> getTransaction(
        @RequestHeader(value = "userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long transactionId) {
        return ResponseEntity.ok(transactionService.getById(transactionId));
    }

    @Operation(summary = "Метод для изменения выбранной операции")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
        description = "Обновленные свойства операции",
        required = true,
        content = @Content(
            schema = @Schema(implementation = TransactionInputDto.class)
        )
    )
    @Parameter(description = "id операции для изменения", name = "id", required = true, example = "1")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionResponseDto> updateTransaction(
        @RequestHeader(value = "userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long transactionId,
        @Valid @RequestBody TransactionInputDto data) {
        return ResponseEntity.ok(transactionService.update(transactionId, data));
    }

    @Operation(summary = "Метод для удаления выбранной операции")
    @Parameter(description = "id операции для удаления", name = "id", required = true, example = "1")
    @DeleteMapping("/{id}")
    public ResponseEntity<TransactionResponseDto> deleteTransaction(
        @RequestHeader(value = "userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long transactionId) {
        return ResponseEntity.ok(transactionService.delete(transactionId));
    }
}
