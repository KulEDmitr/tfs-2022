package com.tinkoff.sirius.financialtracker.controller;

import com.tinkoff.sirius.financialtracker.model.TransactionType;
import com.tinkoff.sirius.financialtracker.model.dto.CategoryInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.CategoryResponseDto;
import com.tinkoff.sirius.financialtracker.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController {

    private final CategoryService categoryService;

    @Operation(summary = "Метод для создания категории операции")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
        description = "Данные категории для создания", required = true,
        content = @Content(
            schema = @Schema(implementation = CategoryInputDto.class)
        )
    )
    public ResponseEntity<CategoryResponseDto> createCategory(
        @RequestHeader(value = "userEmail") String userEmail,
        @Valid @RequestBody CategoryInputDto data) {
        return ResponseEntity.status(HttpStatus.CREATED).body(categoryService.create(data));
    }

    @Operation(summary = "Метод для получения списка всех категорий заданного типа")
    @Parameter(description = "Тип операций, для которых категории должны быть применимы",
        name = "type", required = true, example = "INCOME")
    //(нужна пагинация)
    @GetMapping
    public ResponseEntity<List<CategoryResponseDto>> getAllCategories(
        @RequestHeader(value = "userEmail") String userEmail,
        @RequestParam("type") @NotNull String type) {
        return ResponseEntity.ok(categoryService.getAllByType(TransactionType.valueOf(type)));
    }

    @Operation(summary = "Метод для получения детальной информации по категории")
    @Parameter(description = "id категории для поиска", name = "id", required = true, example = "1")
    @GetMapping("/{id}")
    public ResponseEntity<CategoryResponseDto> getCategory(
        @RequestHeader(value = "userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long id) {
        return ResponseEntity.ok(categoryService.getById(id));
    }

    @Operation(summary = "Метод для получения списка все иконок")
    @GetMapping("/icons")
    public ResponseEntity<List<String>> getIcons() {
        return ResponseEntity.ok(categoryService.getAllIcons());
    }
}
