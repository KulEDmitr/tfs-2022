package com.tinkoff.sirius.financialtracker.controller;

import com.tinkoff.sirius.financialtracker.model.dto.TransactionResponseDto;
import com.tinkoff.sirius.financialtracker.model.dto.WalletInputDto;
import com.tinkoff.sirius.financialtracker.model.dto.WalletResponseDto;
import com.tinkoff.sirius.financialtracker.service.TransactionService;
import com.tinkoff.sirius.financialtracker.service.WalletService;
import com.tinkoff.sirius.financialtracker.model.dto.WalletsTotalResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping(value = "/wallets", produces = MediaType.APPLICATION_JSON_VALUE)
public class WalletController {

    private final WalletService walletService;
    private final TransactionService transactionService;

    @Operation(summary = "Метод для создания кошелька")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
        description = "Кошелек для добавления",
        required = true,
        content = @Content(
            schema = @Schema(implementation = WalletInputDto.class)
        )
    )
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WalletResponseDto> createWallet(
        @RequestHeader(value = "userEmail") String userEmail,
        @Valid @RequestBody WalletInputDto data) {
        return new ResponseEntity<>(walletService.create(data), HttpStatus.CREATED);
    }

    @Operation(summary = "Метод для получения списка всех кошельков пользователя")
    @GetMapping
    public ResponseEntity<List<WalletResponseDto>> getAllWallets(
        @RequestHeader(value = "userEmail") String userEmail) {
        return ResponseEntity.ok(walletService.getAll());
    }

    @Operation(summary = "Метод для получения детальной информации о кошельке")
    @Parameter(description = "id кошелька для поиска", name = "id", required = true, example = "1")
    @GetMapping("/{id}")
    public ResponseEntity<WalletResponseDto> getWallet(
        @RequestHeader(value = "userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long id) {
        return ResponseEntity.ok(walletService.getById(id));
    }

    @Operation(summary = "Метод для получения детальной общей информации по кошельку")
    @GetMapping("/{id}/total")
    public WalletsTotalResponseDto getWalletTotal(
        @RequestHeader(value = "userEmail") String userEmail,
        @Parameter(description = "id кошелька для поиска", name = "id", required = true, example = "1")
        @PathVariable("id") Long id
    ) {
        return walletService.getWalletTotal(id);
    }

    @Operation(summary = "Метод для изменения настроек кошелька")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
        description = "Обновленные свойства кошелька",
        required = true,
        content = @Content(
            schema = @Schema(implementation = WalletInputDto.class)
        )
    )
    @Parameter(description = "id кошелька для обновления", name = "id", required = true, example = "1")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WalletResponseDto> updateWallet(
        @RequestHeader(value = "userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long id,
        @Valid @RequestBody WalletInputDto data) {
        return ResponseEntity.ok(walletService.update(id, data));
    }

    @Operation(summary = "Метод для удаления кошелька")
    @Parameter(description = "id кошелька для удаления", name = "id", required = true, example = "1")
    @DeleteMapping("/{id}")
    public ResponseEntity<WalletResponseDto> deleteWallet(
        @RequestHeader(value = "userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long id) {
        return ResponseEntity.ok(walletService.delete(id));
    }

    @Operation(summary = "Метод для получения списка всех транзакций в выбранном кошельке")
    @Parameter(description = "id кошелька для поиска всех операций в нем",
        name = "id", required = true, example = "1")
    //(нужна пагинация)
    @GetMapping("/{id}/transactions")
    public ResponseEntity<List<TransactionResponseDto>> getAllTransactions(
        @RequestHeader(value = "userEmail") String userEmail,
        @PathVariable("id") @NotNull @Positive Long walletId) {
        return ResponseEntity.ok(transactionService.getAllByWalletId(walletId));
    }

    @Operation(summary = "Метод для получения общей информации по кошелькам")
    @GetMapping("/total")
    public WalletsTotalResponseDto getWalletsTotal(
        @RequestHeader(value = "userEmail") String userEmail) {
        return walletService.getWalletsTotal();
    }
}
