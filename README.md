# Financial Tracker system (Tinkoff Sirius 2022)

***
- [knowledge base](https://www.notion.so/Sirius-2022-80bbe4457b4a44079623b2b06cc8dbd6) on Notion
- [UI design](https://www.figma.com/file/CZIOsnWIjasf6rkzlfdo6J/%D0%9A%D0%BE%D1%88%D0%B5%D0%BB%D0%9E%D0%9A)

### Postgres
#### Requirements
Install [Docker Desktop](https://www.docker.com/products/docker-desktop/).
#### Run
Run command ```docker-compose up --build``` in the root project directory.
